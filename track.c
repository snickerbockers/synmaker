/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of synmaker.
 **
 ** synmaker is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** synmaker is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with synmaker.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "track.h"

struct note
{
    char const* name;
    double freq;
} notes[] = {
    { "C3", 131 },
    { "D3", 147 },
    { "E3", 165 },
    { "F3", 175 },
    { "G3", 196 },
    { "A3", 220 },
    { "B3", 247 },
    { "C4", 262 },
    { "D4", 294 },
    { "E4", 330 },
    { "F4", 350 },
    { "G4", 392 },
    { "A4", 440 },
    { "B4", 494 },
    { "C5", 523 },
    { "D5", 587 },
    { "E5", 659 },
    { "F5", 698 },
    { "G5", 784 },
    { "A5", 880 },
    { "B5", 988 },

    /*
     * XXX - My synth can't handle non-integer frequencies very well
     * (it causes a "popping" noise on note changes), but these are the
     * accurate frequencies according to
     * http://www.phy.mtu.edu/~suits/notefreqs.html
     */
    /* { "C3", 130.81 }, */
    /* { "D3", 146.83 }, */
    /* { "E3", 164.81 }, */
    /* { "F3", 174.61 }, */
    /* { "G3", 196.00 }, */
    /* { "A3", 220.00 }, */
    /* { "B3", 246.94 }, */
    /* { "C4", 261.63 }, */
    /* { "D4", 293.66 }, */
    /* { "E4", 329.63 }, */
    /* { "F4", 349.23 }, */
    /* { "G4", 392.00 }, */
    /* { "A4", 440.00 }, */
    /* { "B4", 493.88 }, */
    /* { "C5", 523.25 }, */
    /* { "D5", 587.33 }, */
    /* { "E5", 659.25 }, */
    /* { "F5", 698.46 }, */
    /* { "G5", 783.99 }, */
    /* { "A5", 880.00 }, */
    /* { "B5", 987.77 }, */

    { NULL, 0.0 }
};

double get_note_freq(char const *name)
{
    struct note *cursor;

    cursor = notes;
    while (cursor->name)
    {
        if (strcmp(cursor->name, name) == 0)
            return cursor->freq;

        cursor++;
    }

    fprintf(stderr, "WARNING: unrecognized note \"%s\"\n", name);
    return 0.0;
}

void add_beat_to_track(struct track *track, char const *note,
                       unsigned duration)
{
    track->beats =
        (struct beat*)realloc(track->beats,
                              sizeof(struct beat) * ++track->n_beats);
    strncpy(track->beats[track->n_beats - 1].note_name, note,
            sizeof(note_str_t) / sizeof(char));
    track->beats[track->n_beats - 1].note_name[NOTE_NAME_LEN] = '\0';
    track->beats[track->n_beats - 1].duration = duration;
}

double sin_synth(double freq, double t)
{
    return (double)sin(t * M_PI * 2.0 * freq);
}

unsigned samples_per_beat(double tempo, unsigned samp_freq)
{
    /*
     * tempo is seconds / beat
     * samp_freq is samples / second
     * need samples_per beat, which should be tempo * samp_freq
     */
    return tempo * samp_freq;
}
