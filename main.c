/*****************************************************************************
 **
 ** Copyright (c) 2016 Jay Elliott
 **
 ** This file is part of synmaker.
 **
 ** synmaker is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** synmaker is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with synmaker.  If not, see <http://www.gnu.org/licenses/>.
 **
 *****************************************************************************/

#include <stdio.h>
#include <string.h>

#include <pulse/simple.h>
#include <pulse/pulseaudio.h>

#include "track.h"

#define SAMPLE_RATE 44100
#define BUF_LEN SAMPLE_RATE

int16_t samp_buf[BUF_LEN];

void read_samples(int16_t *buf, int n_samples, struct track *track)
{
    while (n_samples--)
    {
        *buf++ = track->synth(
            get_note_freq(track->beats[track->current_beat].note_name),
            track->current_beat_sample / (double)SAMPLE_RATE) * INT16_MAX;

        track->current_beat_sample++;
        track->next_sample++;

        if (track->current_beat_sample >=
            samples_per_beat(track->tempo, SAMPLE_RATE) *
            track->beats[track->current_beat].duration)
        {
            track->current_beat_sample = 0;
            track->current_beat = (track->current_beat + 1) % track->n_beats;
        }

        /* printf("n_samples is %d\n", n_samples); */
    }
}

int main(int argc, char **argv)
{
    pa_simple *s;
    pa_sample_spec ss;
    struct track main_track;

    ss.format = PA_SAMPLE_S16LE;
    ss.channels = 1;
    ss.rate = SAMPLE_RATE;
    
    s = pa_simple_new(NULL,
                      "synmaker",
                      PA_STREAM_PLAYBACK,
                      NULL,
                      "bullshit",
                      &ss,
                      NULL,
                      NULL,
                      NULL);

    if (!s)
    {
        fprintf(stderr, "fuck\n");
        return 1;
    }

    strncpy(main_track.name, "track0", TRACK_NAME_LEN);
    main_track.name[TRACK_NAME_LEN-1] = '\0';
    main_track.tempo = 0.5;
    main_track.beats = NULL;
    main_track.n_beats = 0;
    main_track.next_sample = 0;
    main_track.current_beat = 0;
    main_track.current_beat_sample = 0;
    main_track.synth = sin_synth;

    add_beat_to_track(&main_track, "C3", 1);
    add_beat_to_track(&main_track, "D3", 1);
    add_beat_to_track(&main_track, "E3", 1);
    add_beat_to_track(&main_track, "F3", 1);
    add_beat_to_track(&main_track, "G3", 1);
    add_beat_to_track(&main_track, "A3", 1);
    add_beat_to_track(&main_track, "B3", 1);
    add_beat_to_track(&main_track, "C4", 1);
    add_beat_to_track(&main_track, "D4", 1);
    add_beat_to_track(&main_track, "E4", 1);
    add_beat_to_track(&main_track, "F4", 1);
    add_beat_to_track(&main_track, "G4", 1);
    add_beat_to_track(&main_track, "A4", 1);
    add_beat_to_track(&main_track, "B4", 1);
    add_beat_to_track(&main_track, "C5", 1);
    add_beat_to_track(&main_track, "D5", 1);
    add_beat_to_track(&main_track, "E5", 1);
    add_beat_to_track(&main_track, "F5", 1);
    add_beat_to_track(&main_track, "G5", 1);
    add_beat_to_track(&main_track, "A5", 1);
    add_beat_to_track(&main_track, "B5", 1);

    int err_code;
    while (1)
    {
        read_samples(samp_buf, BUF_LEN, &main_track);
        pa_simple_write(s, samp_buf, sizeof(samp_buf), &err_code);
    }

    pa_simple_free(s);

    return 0;
}
